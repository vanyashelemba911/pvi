class ComentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(coment_params)
    redirect_to post_path(@post)
  end

  private def coment_params
    params.require(:comment).permit(:username,:Body)
  end
end
