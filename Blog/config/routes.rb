Rails.application.routes.draw do
  root 'posts#index', as: 'home'
  get 'About' => 'pages#About' , as: 'about'

  resources :posts do
    resources :comments
  end
end
